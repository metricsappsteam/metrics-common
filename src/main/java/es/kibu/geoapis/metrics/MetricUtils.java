package es.kibu.geoapis.metrics;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.MetricsDefinitionReader;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.ReaderInputStream;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;

/**
 * Created by lrodriguez2002cu on 13/09/2016.
 */
public class MetricUtils {

    public static MetricsDefinition withMetricDefinition(String fileName) throws ProcessingException, IOException {
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        return metricsDefinitionReader.readFromResources(MetricUtils.class.getClassLoader(), fileName);
    }

    public static MetricsDefinition withMetricDefinition(InputStream stream) throws ProcessingException, IOException {
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        return metricsDefinitionReader.readFrom(stream);
    }

    public static MetricsDefinition withMetricDefinitionFromContent(String content) throws ProcessingException, IOException {
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        InputStream stream = IOUtils.toInputStream(content, Charset.forName("UTF-8"));
        return metricsDefinitionReader.readFrom(stream);
    }

    public static MetricsDefinition withMetricDefinitionFromContent(Reader content) throws ProcessingException, IOException {
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        InputStream stream = new ReaderInputStream(content, Charset.forName("UTF-8"));
        return metricsDefinitionReader.readFrom(stream);
    }

}
