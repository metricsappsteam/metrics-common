package es.kibu.geoapis.test;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.MetricsDefinitionReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodr_000 on 21/08/2016.
 */
public class TestSupport {


    public static List<MetricsDefinition> getSampleMetrics() {
        List<MetricsDefinition> results = new ArrayList<MetricsDefinition>();
        addMetric(results, "sample-documents/sample_metrics.json");
        addMetric(results, "sample-documents/sample_metrics_multi_scope.json");
        addMetric(results, "sample-documents/sample_metrics_on_text_file.json");

        return results;
    }

    private static void addMetric(List<MetricsDefinition> results, String fileName) {
        try {
            MetricsDefinition metricsDefinition = getMetric(fileName);
            results.add(metricsDefinition);
        } catch (ProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static MetricsDefinition getMetric(String fileName) throws ProcessingException, IOException {
        ClassLoader classLoader = TestSupport.class.getClassLoader();
        File sampleDocFile = new File(classLoader.getResource(fileName).getFile());
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        MetricsDefinition metricsDefinition = metricsDefinitionReader.readFrom(new FileInputStream(sampleDocFile));
        return metricsDefinition;
    }

}
