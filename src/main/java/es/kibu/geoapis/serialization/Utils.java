package es.kibu.geoapis.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import es.kibu.geoapis.objectmodel.CustomDimension;
import es.kibu.geoapis.objectmodel.Dimension;
import es.kibu.geoapis.objectmodel.ProvidedDimension;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 05/08/2016.
 */
public class Utils {


    public static ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Dimension.class, new DimensionDeserializer());
        mapper.registerModule(module);
        return mapper;
    }


    public static class DimensionDeserializer extends JsonDeserializer<Dimension> {
        public Dimension deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

            ObjectCodec mapper = /*(ObjectMapper) */jsonParser.getCodec();
            ObjectNode node = (ObjectNode) mapper.readTree(jsonParser);
            //JsonNode node = jsonParser.readValueAsTree();
            Iterator<Map.Entry<String, JsonNode>> elementsIterator =
                    node.fields();
            Class<? extends Dimension> dimensionClass = null;
            while (elementsIterator.hasNext()) {
                boolean hasName = node.has("name");
                if (hasName) {
                    dimensionClass = CustomDimension.class;
                } else {
                    dimensionClass = ProvidedDimension.class;
                }

            }

            Dimension dimension = mapper.readValue(jsonParser, dimensionClass);

            /*Dimension dimension = null;
            if (node.isObject()) {
                boolean hasName = node.has("name");
                if (hasName) {
                     dimension = jsonParser.readValueAs(Dimension.CustomDimension.class);
                }
                else {
                    dimension = jsonParser.readValueAs(Dimension.ProvidedDimension.class);
                }
            }*/
            return dimension;
        }
    }

   /* public static class DateTimeSerializer extends JsonSerializer<DateTime> {
        @Override
        public void serialize(DateTime dateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeObject(dateTime.toDate());
            //serializerProvider.defaultSerializeDateValue(dateTime.toDate(), jsonGenerator);
        }

    }

    public static class DateTimeDeserializer extends JsonDeserializer<DateTime> {
        @Override
        public DateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            Date t = jsonParser.readValueAs(Date.class);
            DateTime dt = new DateTime(t);
            return dt;
        }
    }*/


    public static ProcessingReport checkJsonSchemaValid(String schema) {
        try {
            JsonNode jsonNode = JsonLoader.fromString(schema);
            return checkJsonSchemaValid(jsonNode);
        } catch ( IOException e) {
            throw new RuntimeException("Invalid schema", e);
        }
    }

    public static ProcessingReport checkJsonSchemaValid(JsonNode jsonNode) {
        ProcessingReport report = JsonSchemaFactory.byDefault().getSyntaxValidator().validateSchema(jsonNode);
        return report;
    }


}
