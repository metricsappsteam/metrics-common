package es.kibu.geoapis.serialization;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.google.gson.Gson;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

import java.io.*;

/**
 * Created by lrodriguez2002cu on 05/08/2016.
 */
public class MetricsDefinitionReader extends BasicMetricsDefinitionReader{

    final JsonSchema schema;

    public MetricsDefinitionReader() throws ProcessingException {
        schema = JsonSchemaFactory.byDefault().getJsonSchema("resource:/metrics-schema.json");
    }

    public InputStream validateInput(InputStream in)  {
        byte[] bytes;
        ProcessingReport processingReport = null;
        try {
            ObjectMapper mapper = Utils.getObjectMapper();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            org.apache.commons.io.IOUtils.copy(in, baos);
            bytes = baos.toByteArray();

            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);

            JsonNode jsonNode = mapper.readTree(bais);
            processingReport = schema.validate(jsonNode);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        if (!processingReport.isSuccess()) throw new RuntimeException(processingReport.toString());

        return new ByteArrayInputStream(bytes);
    }



}
