package es.kibu.geoapis.serialization;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

/**
 * Created by lrodr_000 on 09/08/2016.
 */
public class MetricDefinitionWriterTest {


    private Class getAClass(){
        return MetricsDefinition.class.getClass();
    }

    @Test
    public void someTest() throws IOException, ProcessingException {

        JsonSchema schema = JsonSchemaFactory.byDefault().getJsonSchema("resource:/metrics-schema.json");

        ClassLoader classLoader = getClass().getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream("sample-documents/sample_metrics.json");

        ObjectMapper mapper = Utils.getObjectMapper();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        InputStream in = resourceAsStream;
        org.apache.commons.io.IOUtils.copy(in, baos);
        byte[] bytes = baos.toByteArray();

        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);

        JsonNode jsonNode = mapper.readTree(bais);
        ProcessingReport processingReport = schema.validate(jsonNode);
        assertTrue (processingReport.isSuccess());
        System.out.println(processingReport);

    }

    @Test
    public void testWriter() throws IOException, ProcessingException {
        MetricDefinitionWriter writer = new MetricDefinitionWriter();

        ClassLoader classLoader = getClass().getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream("sample-documents/sample_metrics.json");
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        MetricsDefinition metricsDefinition = metricsDefinitionReader.readFrom(resourceAsStream);

        String json = writer.write(metricsDefinition);
        System.out.println("The  written document is:");
        System.out.println(json);

        MetricsDefinition metricsDefinition1 = metricsDefinitionReader.readFrom(json);
        assertEquals(metricsDefinition, metricsDefinition1);

    }

}