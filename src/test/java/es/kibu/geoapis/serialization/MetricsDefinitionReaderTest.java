package es.kibu.geoapis.serialization;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import es.kibu.geoapis.TestUtils;
import es.kibu.geoapis.objectmodel.Dimension;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lrodr_000 on 09/08/2016.
 */
public class MetricsDefinitionReaderTest {

    public static  final int defaultDimensions = 6;

    private MetricsDefinition withMetrics(String metricsDoc) throws ProcessingException, IOException {
       /* ClassLoader classLoader = getClass().getClassLoader();
        File sampleDocFile = new File(classLoader.getResource(metricsDoc).getFile());
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        return metricsDefinitionReader.readFrom(new FileInputStream(sampleDocFile));*/
        return TestUtils.withMetrics(metricsDoc);
    }

    @Test
    public void testSchemaValid() throws ProcessingException, IOException {

        //Get schemaFile from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream("metrics-schema.json");
        InputStream sample = classLoader.getResourceAsStream("sample-documents/sample_metrics.json");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(resourceAsStream);
        ProcessingReport processingMessages = JsonSchemaFactory.byDefault().getSyntaxValidator().validateSchema(node);
        assertTrue("Validation was success", processingMessages.isSuccess());

        final JsonSchema schema = JsonSchemaFactory.byDefault().getJsonSchema("resource:/metrics-schema.json");

        JsonNode jsonNode = mapper.readTree(sample);
        boolean valid = schema.validInstance(jsonNode);

        assertTrue("Sample document is validated", valid);
    }


    @Test
    public void testSchemaValid1() throws ProcessingException, IOException {

        //Get schemaFile from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream("metrics-schema.json");
        InputStream sample = classLoader.getResourceAsStream("sample-documents/sample-metric-wikiloc-box-speed-geostats.json");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(resourceAsStream);
        ProcessingReport processingMessages = JsonSchemaFactory.byDefault().getSyntaxValidator().validateSchema(node);
        assertTrue("Validation was success", processingMessages.isSuccess());

        final JsonSchema schema = JsonSchemaFactory.byDefault().getJsonSchema("resource:/metrics-schema.json");

        JsonNode jsonNode = mapper.readTree(sample);
        boolean valid = schema.validInstance(jsonNode);

        assertTrue("Sample document is validated", valid);
    }


    @Test
    public void testRead() throws IOException, ProcessingException {

        ClassLoader classLoader = getClass().getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream("sample-documents/sample_metrics.json");
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        MetricsDefinition metricsDefinition = metricsDefinitionReader.readFrom(resourceAsStream);


        //general checks

        assertTrue(metricsDefinition.getVariables().size() == 4);
        assertTrue(metricsDefinition.getScopes().size() == 2);
        assertTrue(metricsDefinition.getActions().size() == 1);
        assertTrue(metricsDefinition.getMetrics().size() == 1);


         /* applicationId, sessionId, time, userId*/;
        //deeper checks
        assertTrue(metricsDefinition.getVariableByName("shootingData").getDimensions().size() == 3 + defaultDimensions);
        assertTrue(metricsDefinition.getVariableByName("campaignData").getDimensions().size() == 2 + defaultDimensions);
        assertTrue(metricsDefinition.getVariableByName("actionData").getDimensions().size() == 4 + defaultDimensions);


        //deeper checks
        List<Dimension> shootingData = metricsDefinition.getVariableByName("shootingData").getDimensions();

        DimensionStats dimensionStats = new DimensionStats(shootingData).invoke();
        int provided = dimensionStats.getProvided();
        int custom = dimensionStats.getCustom();

        assertTrue(provided == (1 + defaultDimensions));
        assertTrue(custom == 2);

        List<Dimension> campaignData = metricsDefinition.getVariableByName("campaignData").getDimensions();
        dimensionStats = new DimensionStats(campaignData).invoke();

        assertTrue(dimensionStats.getCustom() == 2);
        assertTrue(dimensionStats.getProvided() == (0 + defaultDimensions));


        List<Dimension> actionData = metricsDefinition.getVariableByName("actionData").getDimensions();
        dimensionStats = new DimensionStats(actionData).invoke();

        assertTrue(dimensionStats.getCustom() == 1);
        assertTrue(dimensionStats.getProvided() == (3 + defaultDimensions));

        for (Metric metric : metricsDefinition.getMetrics()) {
            assertTrue(metric.getOutput()!=null);
            assertTrue(metric.getOutput().getSchema()!=null);
        }

    }

    private class DimensionStats {

        private List<Dimension> shootingData;
        private int provided;
        private int custom;

        public DimensionStats(List<Dimension> shootingData) {
            this.shootingData = shootingData;
        }

        public int getProvided() {
            return provided;
        }

        public int getCustom() {
            return custom;
        }

        public DimensionStats invoke() {
            provided = 0;
            custom = 0;

            for (Dimension dimension : shootingData) {
                if (dimension.isCustomDimension()) custom++;
                if (dimension.isProvided()) provided++;

            }
            return this;
        }
    }
}