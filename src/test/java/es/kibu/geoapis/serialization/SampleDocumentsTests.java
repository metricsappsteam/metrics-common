package es.kibu.geoapis.serialization;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import org.junit.Test;

/**
 * Created by lrodriguez2002cu on 16/01/2017.
 */
public class SampleDocumentsTests {



    public static final String [] sampleDocuments = {
            "sample-documents/sample-metric.json",
            "sample-documents/sample-metric-orientation.json",
            "sample-documents/sample-metric-wikiloc-box-speed-geostats.json",
            "sample-documents/sample-metric_classprovider.json",
            "sample-documents/sample_metrics.json",
            "sample-documents/sample_metrics_custom_object.json",
            "sample-documents/sample_metrics_multi_scope.json",
            "sample-documents/sample_metrics_on_text_file.json"

    };

    @Test
    public void checkResourcesValid() throws ProcessingException {

         MetricsDefinitionReader reader = new MetricsDefinitionReader();
        for (String sampleDocument : sampleDocuments) {
            System.out.println("Testing document:" + sampleDocument);
            reader.readFromResources(SampleDocumentsTests.class.getClassLoader(), sampleDocument);
            System.out.println("End testing document" + sampleDocument);
        }

    }
}
