package es.kibu.geoapis;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.MetricsDefinitionReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by lrodr_000 on 07/09/2016.
 */
public class TestUtils {

    public static MetricsDefinition withMetrics(String metricsDoc) throws ProcessingException, IOException {
        ClassLoader classLoader = TestUtils.class.getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream(metricsDoc);
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        return metricsDefinitionReader.readFrom(resourceAsStream);
    }
}
